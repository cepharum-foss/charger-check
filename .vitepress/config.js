import { viteStaticCopy } from "vite-plugin-static-copy";

export default {
	lang: "de-DE",
	title: "Charger-Check",
	description: "Tarifvergleich der Ladenetzbetreiber",
	outDir: "public",
	base: "/charger-check/",

	head: [
		[ "link", { rel: "icon", href: "/favicon.svg" } ],
	],

	vue: {
		template: {
			compilerOptions: {
				isCustomElement: tag => tag.startsWith( "sl-" )
			}
		}
	},
	vite: {
		plugins: [
			viteStaticCopy( {
				targets: [
					{
						src: "node_modules/@shoelace-style/shoelace/dist/assets/",
						dest: "shoelace/",
					}
				]
			} ),
		],
	},
};
