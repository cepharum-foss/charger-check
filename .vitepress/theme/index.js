import DefaultTheme from "vitepress/theme";
import { defineAsyncComponent } from "vue";
import { useThemeSync } from "@cepharum/vue3-utils/dist/vue3-utils.js";

import "./style.scss";

import { setBasePath } from "@shoelace-style/shoelace/dist/utilities/base-path.js";
import "@shoelace-style/shoelace/dist/themes/light.css";
import "@shoelace-style/shoelace/dist/themes/dark.css";

setBasePath( import.meta.env.BASE_URL + "shoelace/" );

const components = import.meta.glob( "./components/**/*.vue" );

export default {
	extends: DefaultTheme,
	async enhanceApp( { app } ) {
		setBasePath( import.meta.env.BASE_URL + "shoelace/" );

		// make sure to import Shoelace components in an actual browser, only
		if ( typeof document !== "undefined" ) {
			await Promise.all( [
				import( "@shoelace-style/shoelace/dist/components/dialog/dialog.js" ),
				import( "@shoelace-style/shoelace/dist/components/button/button.js" ),
				import( "@shoelace-style/shoelace/dist/components/format-number/format-number.js" ),
				import( "@shoelace-style/shoelace/dist/components/icon/icon.js" ),
				import( "@shoelace-style/shoelace/dist/components/icon-button/icon-button.js" ),
				import( "@shoelace-style/shoelace/dist/components/range/range.js" ),
				import( "@shoelace-style/shoelace/dist/components/switch/switch.js" ),
				import( "@shoelace-style/shoelace/dist/components/tag/tag.js" ),
				import( "@shoelace-style/shoelace/dist/components/tooltip/tooltip.js" ),
			] );
		}

		for ( const [ name, component ] of Object.entries( components ) ) {
			app.component( name.replace( /^.+\/|\.vue$/g, "" ), defineAsyncComponent( component ) );
		}

		useThemeSync();
	}
};
