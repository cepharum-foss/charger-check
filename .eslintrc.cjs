/* eslint-env node */
module.exports = {
	root: true,
	extends: [
		"plugin:vue/vue3-essential",
		"eslint-config-cepharum",
	],
	parserOptions: {
		ecmaVersion: "latest"
	}
};
